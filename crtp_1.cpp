
#include <iostream>


// (STATIC) INTERFACE:

template<typename T>
class BaseClass
{
public:

   void Print()
   {
      std::cout << "I'm the polymorphic method! -> ";

      T& p = static_cast<T&>( *this );
      p.greetings();

      // could have been written also:
      //    static_cast<T&>( *this ).print();
      // or:
      //    static_cast<T*>( this )->Print();

   }

protected:

   BaseClass() = default;
   // hide the constructors from the clients

};


// IMPLEMENTATION 1:

class SubClass1 : public BaseClass<SubClass1>
{
public:
   void greetings()
   {
      std::cout << "Hello world\n";
   }
};


// IMPLEMENTATION 2:

class SubClass2 : public BaseClass<SubClass2>
{
public:
   void greetings()
   {
      std::cout << "Hola mundo\n";
   }
};



int main()
{
   SubClass1 subClass1;
   SubClass2 subClass2;

   subClass1.Print();
   subClass2.Print();
}
